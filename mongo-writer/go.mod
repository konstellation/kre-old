module gitlab.com/konstellation/kre/mongo-writer

go 1.13

require (
	github.com/golang/protobuf v1.4.0 // indirect
	github.com/kelseyhightower/envconfig v1.4.0
	github.com/nats-io/nats-server/v2 v2.1.6 // indirect
	github.com/nats-io/nats.go v1.9.2
	gitlab.com/konstellation/kre/libs/simplelogger v0.0.0-20200428100928-adb9f31d75ab
	go.mongodb.org/mongo-driver v1.3.2
	gopkg.in/yaml.v2 v2.2.8
)
